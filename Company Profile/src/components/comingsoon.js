import React, { Component } from 'react';

export  default class ComingSoon extends Component {
	render() {
		return(
		<div>
	<div className="logo-image">                                
       <a href="index.html"><img className="img-responsive" src="images/logo.png" alt="" /> </a> 
    </div>
     <section id="coming-soon">        
         <div className="container">
		 
                <div className="col-sm-8 col-sm-offset-2">                    
                    <div className="text-center coming-content">
                        <h1>UNDER CONSTRUCTION</h1>
                        <p>We have been spending long hours in order to launch our new website. 
                            We will offer freebies, a brand new blog and featured content of our latest work. Join our mailing list or follow us on<br /> Facebook or Twitter to stay up to date.</p>                           
                        <div className="social-link">
                            <span><a href="#"><i className="fa fa-facebook"></i></a></span>
                            <span><a href="#"><i className="fa fa-twitter"></i></a></span>
                            <span><a href="#"><i className="fa fa-google-plus"></i></a></span>
                        </div>
                    </div>                    
                </div>
                
        </div>       
    </section>
    <section id="subscribe">
        <div className="container">
            <div className="row">
                <div className="col-sm-10 col-sm-offset-1">
                    <div className="row">
                        <div className="col-sm-6">
                            <h2><i className="fa fa-envelope-o"></i> SUBSCRIBE TO OUR NEWSLETTER</h2>
                            <p>Quis filet mignon proident, laboris venison tri-tip commodo brisket aute ut. Tail salami pork belly, flank ullamco bacon bresaola do beef<br /> laboris venison tri-tip.</p>
                        </div>
                        <div className="col-sm-6 newsletter">
                            <form id="newsletter">
                                <input className="form-control" type="email" name="email"  value="" placeholder="Enter Your email" />
                                <i className="fa fa-check"></i>
                            </form>
                            <p>Don't worry we will not use your email for spam</p>
                        </div>    
                    </div>
                </div>     
            </div>
        </div> 
    </section>

    <section id="coming-soon-footer" className="container">
        <div className="row">
            <div className="col-sm-12">
                <div className="text-center">
                    <p>&copy; Fiture 2018. All Rights Reserved.</p>
                </div>
            </div>
        </div>       
    </section>
	
    </div>
		);
	}
}