import React, { Component } from 'react';

export default class Header extends Component{
	render(){
		return(
			<header id="header">      
				<div className="container">
					<div className="row">
						<div className="col-sm-12 overflow">
						   <div className="social-icons pull-right">
								<ul className="nav nav-pills">
									<li><a href="http://www.facebook.com"><i className="fa fa-facebook"></i></a></li>
									<li><a href="http://www.twitter.com"><i className="fa fa-twitter"></i></a></li>
									<li><a href=""><i className="fa fa-instagram"></i></a></li>
									<li><a href=""><i className="fa fa-linkedin"></i></a></li>
								</ul>
							</div> 
						</div>
					 </div>
				</div>
				<div className="navbar navbar-inverse" role="banner">
					<div className="container">
						<div className="navbar-header">
							<button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span className="sr-only">Toggle navigation</span>
								<span className="icon-bar"></span>
								<span className="icon-bar"></span>
								<span className="icon-bar"></span>
							</button>

							<a className="navbar-brand" href="/">
								<h1><img src="images/logo.png" alt="logo" width='200px' /></h1>
							</a>
							
						</div>
						<div className="collapse navbar-collapse">
							<ul className="nav navbar-nav navbar-right">
								<li><a href="/">Home</a></li>
								<li><a href="/aboutus">About Us</a></li>
								<li className="dropdown"><a href="#">Services</a>
									<ul role="menu" className="sub-menu">
										<li><a href='http://103.82.241.18/'>E - Commerce</a></li>
									</ul>
								</li>
								<li><a href="/projectplanner">Project Planner</a></li>
								<li><a href="/timeline">Timeline</a></li>
								<li><a href="/contact">Contact us</a></li>
									
							</ul>
						</div>
						<div className="search">
							<form role="form">
								<i className="fa fa-search"></i>
								<div className="field-toggle">
									<input type="text" className="search-form" autocomplete="off" placeholder="Search" />
								</div>
							</form>
						</div>
					</div>
				</div>
			</header>
		);
	}
}