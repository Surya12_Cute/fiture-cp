import React, { Component } from 'react';

export default class NotFound extends Component {
	render() {
		return(
			<section id="error-page">
				<div className="error-page-inner">
					<div className="container-fluid">
						<div className="row">
							<div className="col-sm-12">
								<div className="text-center">
									<div className="bg-404">
										<div className="error-image">                                
											<img className="img-responsive" src="images/404.png" alt="" />  
										</div>
									</div>
									<h2>PAGE NOT FOUND</h2>
									<p>The page you are looking for might have been removed, had its name changed.</p>
									<a href="/" className="btn btn-error">RETURN TO THE HOMEPAGE</a>
									<div className="social-link">
										<span><a href="#"><i className="fa fa-facebook"></i></a></span>
										<span><a href="#"><i className="fa fa-twitter"></i></a></span>
										<span><a href="#"><i className="fa fa-google-plus"></i></a></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		);
	}
}