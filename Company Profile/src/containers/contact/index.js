import React, { Component } from 'react';

export default class Contact extends Component {
	render() {
		return(
			<div>
				<section id="page-breadcrumb">
					<div className="vertical-center sun">
						 <div className="container">
							<div className="row">
								<div className="action">
									<div className="col-sm-12">
										<h1 className="title">Contact US</h1>
										<p>Stay close</p>
									</div>                        
								</div>
							</div>
						</div>
					</div>
			   </section>

				<section id="map-section">
					<div className="container">
						<div id="gmap"></div>
					</div>
				</section>
				<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
			</div>
		
		);
	}
}