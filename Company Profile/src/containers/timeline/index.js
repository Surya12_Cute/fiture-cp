import React, { Component } from 'react';

export default class Timeline extends Component {
	render() {
		return(
			<div>
			<section id="page-breadcrumb">
				<div className="vertical-center sun">
					 <div className="container">
						<div className="row">
							<div className="action">
								<div className="col-sm-12">
									<h1 className="title">Blog</h1>
									<p>Timeline</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			
			<section id="blog" className="padding-bottom">
				<div className="container">
					<div className="row">
						<div className="timeline-blog overflow padding-top">
							<div className="timeline-date text-center">
								<a href="#" className="btn btn-common uppercase">November 2013</a>
							</div>
							<div className="timeline-divider overflow padding-bottom">
								<div className="col-sm-6 padding-right arrow-right wow fadeInLeft" data-wow-duration="1000ms" data-wow-delay="300ms">
									<div className="single-blog timeline">
										<div className="single-blog-wrapper">
											<div className="post-thumb">
												<img src="images/blog/timeline/1.jpg" className="img-responsive" alt="" />
												<div className="post-overlay">
												   <span className="uppercase"><a href="#">14 <br /><small>Feb</small></a></span>
											   </div>
											</div>
										</div>
										<div className="post-content overflow">
											<h2 className="post-title bold"><a href="blogdetails.html">Advanced business cards design</a></h2>
											<h3 className="post-author"><a href="#">Posted by micron News</a></h3>
											<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber [...]</p>
											<a href="#" className="read-more">View More</a>
											<div className="post-bottom overflow">
												<span className="post-date pull-left">February 11, 2014</span>
												<span className="comments-number pull-right"><a href="#">3 comments</a></span>
											</div>
										</div>
									</div>
								</div>
								<div className="col-sm-6 padding-left padding-top arrow-left wow fadeInRight" data-wow-duration="1000ms" data-wow-delay="300ms">
									<div className="single-blog timeline">
										<div className="single-blog-wrapper">
											<div className="post-thumb">
												<img src="images/blog/timeline/2.jpg" className="img-responsive" alt="" />
												<div className="post-overlay">
												   <span className="uppercase"><a href="#">14 <br /><small>Feb</small></a></span>
											   </div>
											</div>
										</div>
										<div className="post-content overflow">
											<h2 className="post-title bold"><a href="blogdetails.html#">Advanced business cards design</a></h2>
											<h3 className="post-author"><a href="#">Posted by micron News</a></h3>
											<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber [...]</p>
											<a href="#" className="read-more">View More</a>
											<div className="post-bottom overflow">
												<span className="post-date pull-left">February 11, 2014</span>
												<span className="comments-number pull-right"><a href="#">3 comments</a></span>
											</div>
										</div>
									</div>
								</div>
								<div className="col-sm-6 padding-right arrow-right wow fadeInLeft" data-wow-duration="1000ms" data-wow-delay="300ms">
									<div className="single-blog timeline">
										<div className="single-blog-wrapper">
											<div className="post-thumb">
												<iframe src="https://player.vimeo.com/video/95995577"></iframe>
											</div>
										</div>
										<div className="post-content overflow">
											<h2 className="post-title bold"><a href="blogdetails.html">Advanced business cards design</a></h2>
											<h3 className="post-author"><a href="#">Posted by micron News</a></h3>
											<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber [...]</p>
											<a href="#" className="read-more">View More</a>
											<div className="post-bottom overflow">
												<span className="post-date pull-left">February 11, 2014</span>
												<span className="comments-number pull-right"><a href="#">3 comments</a></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div className="timeline-blog overflow">
							<div className="timeline-date text-center">
								<a href="" className="btn btn-common uppercase">Setember 2013</a>
							</div>
							<div className="timeline-divider overflow padding-bottom">
								<div className="col-sm-6 padding-right arrow-right wow fadeInLeft" data-wow-duration="1000ms" data-wow-delay="300ms">
									<div className="single-blog timeline">
										<div className="single-blog-wrapper">
											<div className="post-thumb">
												<img src="images/blog/timeline/3.jpg" className="img-responsive" alt="" />
												<div className="post-overlay">
												   <span className="uppercase"><a href="#">14 <br /><small>Feb</small></a></span>
											   </div>
											</div>
										</div>
										<div className="post-content overflow">
											<h2 className="post-title bold"><a href="blogdetails.html">Advanced business cards design</a></h2>
											<h3 className="post-author"><a href="#">Posted by micron News</a></h3>
											<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber [...]</p>
											<a href="#" className="read-more">View More</a>
											<div className="post-bottom overflow">
												<span className="post-date pull-left">February 11, 2014</span>
												<span className="comments-number pull-right"><a href="#">3 comments</a></span>
											</div>
										</div>
									</div>
								</div>
								<div className="col-sm-6 padding-left padding-top arrow-left wow fadeInRight" data-wow-duration="1000ms" data-wow-delay="300ms">
									<div className="single-blog timeline">
										<div className="single-blog-wrapper">
											<div className="post-thumb">
												<img src="images/blog/timeline/4.jpg" className="img-responsive" alt="" />
												<div className="post-overlay">
												   <span className="uppercase"><a href="#">14 <br /><small>Feb</small></a></span>
											   </div>
											</div>
										</div>
										<div className="post-content overflow">
											<h2 className="post-title bold"><a href="blogdetails.html">Advanced business cards design</a></h2>
											<h3 className="post-author"><a href="#">Posted by micron News</a></h3>
											<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber [...]</p>
											<a href="#" className="read-more">View More</a>
											<div className="post-bottom overflow">
												<span className="post-date pull-left">February 11, 2014</span>
												<span className="comments-number pull-right"><a href="#">3 comments</a></span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div className="timeline-date text-center">
								<a href="#" className="btn btn-common">See More</a>
							</div>
						</div>
					</div>
				</div>
			</section>
			
			</div>
		);
	}
}