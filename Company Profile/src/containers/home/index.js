import React, { Component } from 'react';

export default class Home_Component extends Component {
	render(){
		return(
			<div>
			<section id="home-slider">
				<div className="container">
					<div className="row">
						<div className="main-slider">
							<div className="slide-text">
								<h1>We Are Creative Nerds</h1>
								<p>Boudin doner frankfurter pig. Cow shank bresaola pork loin tri-tip tongue venison pork belly meatloaf short loin landjaeger biltong beef ribs shankle chicken andouille.</p>
								<a href="http://103.82.241.18/" className="btn btn-common">E - Commerce</a>
							</div>
							<img src="images/home/slider/hill.png" className="slider-hill" alt="slider image" />
							<img src="images/home/slider/house.png" className="slider-house" alt="slider image" />
							<img src="images/home/slider/sun.png" className="slider-sun" alt="slider image" />
							<img src="images/home/slider/birds1.png" className="slider-birds1" alt="slider image" />
							<img src="images/home/slider/birds2.png" className="slider-birds2" alt="slider image" />
						</div>
					</div>
				</div>
				<div className="preloader"><i className="fa fa-sun-o fa-spin"></i></div>
			</section>
			

			<section id="services">
				<div className="container">
					<div className="row">
						<div className="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
							<div className="single-service">
								<div className="wow scaleIn" data-wow-duration="500ms" data-wow-delay="300ms">
									<img src="images/home/icon1.png" alt="" />
								</div>
								<h2>Incredibly Responsive</h2>
								<p>Ground round tenderloin flank shank ribeye. Hamkevin meatball swine. Cow shankle beef sirloin chicken ground round.</p>
							</div>
						</div>
						<div className="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
							<div className="single-service">
								<div className="wow scaleIn" data-wow-duration="500ms" data-wow-delay="600ms">
									<img src="images/home/icon2.png" alt="" />
								</div>
								<h2>Superior Typography</h2>
								<p>Hamburger ribeye drumstick turkey, strip steak sausage ground round shank pastrami beef brisket pancetta venison.</p>
							</div>
						</div>
						<div className="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="900ms">
							<div className="single-service">
								<div className="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
									<img src="images/home/icon3.png" alt="" />
								</div>
								<h2>Swift Page Builder</h2>
								<p>Venison tongue, salami corned beef ball tip meatloaf bacon. Fatback pork belly bresaola tenderloin bone pork kevin shankle.</p>
							</div>
						</div>
					</div>
				</div>
			</section>
			

			<section id="action" className="responsive">
				<div className="vertical-center">
					 <div className="container">
						<div className="row">
							<div className="action take-tour">
								<div className="col-sm-7 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
									<h1 className="title">Triangle Corporate Template</h1>
									<p>A responsive, retina-ready &amp; wide multipurpose template.</p>
								</div>
								<div className="col-sm-5 text-center wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
									<div className="tour-button">
										<a href="#" className="btn btn-common">TAKE THE TOUR</a>
									 </div>
								</div>
							</div>
						</div>
					</div>
				</div>
		   </section>
			

			<section id="features">
				<div className="container">
					<div className="row">
						<div className="single-features">
							<div className="col-sm-5 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
								<img src="images/home/image1.png" className="img-responsive" alt="" />
							</div>
							<div className="col-sm-6 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
								<h2>Experienced and Enthusiastic</h2>
								<p>Pork belly leberkas cow short ribs capicola pork loin. Doner fatback frankfurter jerky meatball pastrami bacon tail sausage. Turkey fatback ball tip, tri-tip tenderloin drumstick salami strip steak.</p>
							</div>
						</div>
						<div className="single-features">
							<div className="col-sm-6 col-sm-offset-1 align-right wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
								<h2>Built for the Responsive Web</h2>
								<p>Mollit eiusmod id chuck turducken laboris meatloaf pork loin tenderloin swine. Pancetta excepteur fugiat strip steak tri-tip. Swine salami eiusmod sint, ex id venison non. Fugiat ea jowl cillum meatloaf.</p>
							</div>
							<div className="col-sm-5 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
								<img src="images/home/image2.png" className="img-responsive" alt="" />
							</div>
						</div>
						<div className="single-features">
							<div className="col-sm-5 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
								<img src="images/home/image3.png" className="img-responsive" alt="" />
							</div>
							<div className="col-sm-6 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
								<h2>Experienced and Enthusiastic</h2>
								<p>Ut officia cupidatat anim excepteur fugiat cillum ea occaecat rump pork chop tempor. Ut tenderloin veniam commodo. Shankle aliquip short ribs, chicken eiusmod exercitation shank landjaeger spare ribs corned beef.</p>
							</div>
						</div>
					</div>
				</div>
			</section>
			 

			<section id="clients">
				<div className="container">
					<div className="row">
						<div className="col-sm-12">
							<div className="clients text-center wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms">
								<p><img src="images/home/clients.png" className="img-responsive" alt="" /></p>
								<h1 className="title">Happy Clients</h1>
								<p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br /> Ut enim ad minim veniam </p>
							</div>
							<div className="clients-logo wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
								<div className="col-xs-3 col-sm-2">
									<a href="#"><img src="images/home/client1.png" className="img-responsive" alt="" /></a>
								</div>
								<div className="col-xs-3 col-sm-2">
									<a href="#"><img src="images/home/client2.png" className="img-responsive" alt="" /></a>
								</div>
								 <div className="col-xs-3 col-sm-2">
									<a href="#"><img src="images/home/client3.png" className="img-responsive" alt="" /></a>
								</div>
								 <div className="col-xs-3 col-sm-2">
									<a href="#"><img src="images/home/client4.png" className="img-responsive" alt="" /></a>
								</div>
								 <div className="col-xs-3 col-sm-2">
									<a href="#"><img src="images/home/client5.png" className="img-responsive" alt="" /></a>
								</div>
								 <div className="col-xs-3 col-sm-2">
									<a href="#"><img src="images/home/client6.png" className="img-responsive" alt="" /></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			 </section>
			</div>
		);
	}
}