import React, { Component } from 'react';

export default class Aboutus_Component extends Component {
	render() {
		return(
			<div>
				<section id="page-breadcrumb">
					<div className="vertical-center sun">
						 <div className="container">
							<div className="row">
								<div className="action">
									<div className="col-sm-12">
										<h1 className="title">About Us</h1>
										<p>Why our Clients love to work with us.</p>
									</div>
								 </div>
							</div>
						</div>
					</div>
			   </section>
				

				<section id="company-information" className="padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
					<div className="container">
						<div className="row">
							<div className="col-sm-6">
								<img src="images/aboutus/1.png" className="img-responsive" alt="" />
							</div>
							<div className="col-sm-6 padding-top">
								<p>Shoulder bresaola sausage consequat ground round duis excepteur exercitation landjaeger sunt. Duis officia sed frankfurter dolore pastrami tenderloin.</p>
								<p>When an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
							</div>
						</div>
					</div>
				</section>

				<section id="services">
					<div className="container">
						<div className="row">
							<div className="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
								<div className="single-service">
									<div className="wow scaleIn" data-wow-duration="500ms" data-wow-delay="300ms">
										<img src="images/home/icon1.png" alt="" />
									</div>
									<h2>Incredibly Responsive</h2>
									<p>Ground round tenderloin flank shank ribeye. Hamkevin meatball swine. Cow shankle beef sirloin chicken ground round.</p>
								</div>
							</div>
							<div className="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
								<div className="single-service">
									<div className="wow scaleIn" data-wow-duration="500ms" data-wow-delay="600ms">
										<img src="images/home/icon2.png" alt="" />
									</div>
									<h2>Superior Typography</h2>
									<p>Hamburger ribeye drumstick turkey, strip steak sausage ground round shank pastrami beef brisket pancetta venison.</p>
								</div>
							</div>
							<div className="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="900ms">
								<div className="single-service">
									<div className="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
										<img src="images/home/icon3.png" alt="" />
									</div>
									<h2>Swift Page Builder</h2>
									<p>Venison tongue, salami corned beef ball tip meatloaf bacon. Fatback pork belly bresaola tenderloin bone pork kevin shankle.</p>
								</div>
							</div>
						</div>
					</div>
				</section>
				

				<section id="action" className="responsive">
					<div className="vertical-center">
						 <div className="container">
							<div className="row">
								<div className="action take-tour">
									<div className="col-sm-7 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
										<h1 className="title">Triangle Corporate Template</h1>
										<p>A responsive, retina-ready &amp; wide multipurpose template.</p>
									</div>
									<div className="col-sm-5 text-center wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
										<div className="tour-button">
											<a href="#" className="btn btn-common">TAKE THE TOUR</a>
										 </div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				
				/*
				<section id="team">
					<div className="container">
						<div className="row">
							<h1 className="title text-center wow fadeInDown" data-wow-duration="500ms" data-wow-delay="300ms">Meet the Team</h1>
							<p className="text-center wow fadeInDown" data-wow-duration="400ms" data-wow-delay="400ms">Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br />
							Ut enim ad minim veniam, quis nostrud </p>
							<div id="team-carousel" className="carousel slide wow fadeIn" data-ride="carousel" data-wow-duration="400ms" data-wow-delay="400ms">
											Indicators
								<ol className="carousel-indicators visible-xs">
									<li data-target="#team-carousel" data-slide-to="0" className="active"></li>
									<li data-target="#team-carousel" data-slide-to="1"></li>
								</ol>
											Wrapper for slides
								<div className="carousel-inner">
									<div className="item active">
										<div className="col-sm-3 col-xs-6">
											<div className="team-single-wrapper">
												<div className="team-single">
													<div className="person-thumb">
														<img src="images/aboutus/1.jpg" className="img-responsive" alt="" />
													</div>
													<div className="social-profile">
														<ul className="nav nav-pills">
															<li><a href="#"><i className="fa fa-facebook"></i></a></li>
															<li><a href="#"><i className="fa fa-twitter"></i></a></li>
															<li><a href="#"><i className="fa fa-google-plus"></i></a></li>
														</ul>
													</div>
												</div>
												<div className="person-info">
													<h2>John Doe</h2>
													<p>CEO &amp; Developer</p>
												</div>
											</div>
										</div>
										<div className="col-sm-3 col-xs-6">
											<div className="team-single-wrapper">
												<div className="team-single">
													<div className="person-thumb">
														<img src="images/aboutus/2.jpg" className="img-responsive" alt="" />
													</div>
													<div className="social-profile">
														<ul className="nav nav-pills">
															<li><a href="#"><i className="fa fa-facebook"></i></a></li>
															<li><a href="#"><i className="fa fa-twitter"></i></a></li>
															<li><a href="#"><i className="fa fa-google-plus"></i></a></li>
														</ul>
													</div>
												</div>
												<div className="person-info">
													<h2>John Doe</h2>
													<p>CEO &amp; Developer</p>
												</div>
											</div>
										</div>
										<div className="col-sm-3 col-xs-6">
											<div className="team-single-wrapper">
												<div className="team-single">
													<div className="person-thumb">
														<img src="images/aboutus/3.jpg" className="img-responsive" alt="" />
													</div>
													<div className="social-profile">
														<ul className="nav nav-pills">
															<li><a href="#"><i className="fa fa-facebook"></i></a></li>
															<li><a href="#"><i className="fa fa-twitter"></i></a></li>
															<li><a href="#"><i className="fa fa-google-plus"></i></a></li>
														</ul>
													</div>
												</div>
												<div className="person-info">
													<h2>John Doe</h2>
													<p>CEO &amp; Developer</p>
												</div>
											</div>
										</div>
										<div className="col-sm-3 col-xs-6">
											<div className="team-single-wrapper">
												<div className="team-single">
													<div className="person-thumb">
														<img src="images/aboutus/1.jpg" className="img-responsive" alt="" />
													</div>
													<div className="social-profile">
														<ul className="nav nav-pills">
															<li><a href="#"><i className="fa fa-facebook"></i></a></li>
															<li><a href="#"><i className="fa fa-twitter"></i></a></li>
															<li><a href="#"><i className="fa fa-google-plus"></i></a></li>
														</ul>
													</div>
												</div>
												<div className="person-info">
													<h2>John Doe</h2>
													<p>CEO &amp; Developer</p>
												</div>
											</div>
										</div>
									</div>
									<div className="item">
										<div className="col-sm-3 col-xs-6">
											<div className="team-single-wrapper">
												<div className="team-single">
													<div className="person-thumb">
														<img src="images/aboutus/4.jpg" className="img-responsive" alt="" />
													</div>
													<div className="social-profile">
														<ul className="nav nav-pills">
															<li><a href="#"><i className="fa fa-facebook"></i></a></li>
															<li><a href="#"><i className="fa fa-twitter"></i></a></li>
															<li><a href="#"><i className="fa fa-google-plus"></i></a></li>
														</ul>
													</div>
												</div>
												<div className="person-info">
													<h2>John Doe</h2>
													<p>CEO &amp; Developer</p>
												</div>
											</div>
										</div>
										<div className="col-sm-3 col-xs-6">
											<div className="team-single-wrapper">
												<div className="team-single">
													<div className="person-thumb">
														<img src="images/aboutus/3.jpg" className="img-responsive" alt="" />
													</div>
													<div className="social-profile">
														<ul className="nav nav-pills">
															<li><a href="#"><i className="fa fa-facebook"></i></a></li>
															<li><a href="#"><i className="fa fa-twitter"></i></a></li>
															<li><a href="#"><i className="fa fa-google-plus"></i></a></li>
														</ul>
													</div>
												</div>
												<div className="person-info">
													<h2>John Doe</h2>
													<p>CEO &amp; Developer</p>
												</div>
											</div>
										</div>
										<div className="col-sm-3 col-xs-6">
											<div className="team-single-wrapper">
												<div className="team-single">
													<div className="person-thumb">
														<img src="images/aboutus/2.jpg" className="img-responsive" alt="" />
													</div>
													<div className="social-profile">
														<ul className="nav nav-pills">
															<li><a href="#"><i className="fa fa-facebook"></i></a></li>
															<li><a href="#"><i className="fa fa-twitter"></i></a></li>
															<li><a href="#"><i className="fa fa-google-plus"></i></a></li>
														</ul>
													</div>
												</div>
												<div className="person-info">
													<h2>John Doe</h2>
													<p>CEO &amp; Developer</p>
												</div>
											</div>
										</div>
										<div className="col-sm-3 col-xs-6">
											<div className="team-single-wrapper">
												<div className="team-single">
													<div className="person-thumb">
														<img src="images/aboutus/1.jpg" className="img-responsive" alt="" />
													</div>
													<div className="social-profile">
														<ul className="nav nav-pills">
															<li><a href="#"><i className="fa fa-facebook"></i></a></li>
															<li><a href="#"><i className="fa fa-twitter"></i></a></li>
															<li><a href="#"><i className="fa fa-google-plus"></i></a></li>
														</ul>
													</div>
												</div>
												<div className="person-info">
													<h2>John Doe</h2>
													<p>CEO &amp; Developer</p>
												</div>
											</div>
										</div>
									</div>
								</div>
								Controls 
								<a className="left team-carousel-control hidden-xs" href="#team-carousel" data-slide="prev">left</a>
								<a className="right team-carousel-control hidden-xs" href="#team-carousel" data-slide="next">right</a>
							</div>
						</div>
					</div>
				</section>
				
				*/
			</div>
		);
	}
}