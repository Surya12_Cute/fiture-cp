import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Header from './components/header';
import Footer from './components/footer';

import Home_Component from './containers/home/index';
import Aboutus_Component from './containers/aboutus/index';
import Contact from './containers/contact/index';
import Timeline from './containers/timeline/index';

import ComingSoon from './components/comingsoon';
import NotFound from './components/notfound';

class Blog extends Component {
	render() {
		return (
			<body>
				<Header />
					<BrowserRouter>
						<div>
							<Route exact path='/' component={Home_Component}  />
							<Route path='/aboutus' component={Aboutus_Component} />
							<Route path='/contact' component={Contact} />
							<Route path='/timeline' component={Timeline} />
						</div>
					</BrowserRouter>
				<Footer  /> 
			</body>
		);
	}
}

ReactDOM.render(
		<BrowserRouter>
			<div>
				<Switch>
					<Route exact path='/' component={Blog}  />
					<Route path='/aboutus' component={Blog}  />
					<Route path='/contact' component={Blog}  />
					<Route path='/timeline' component={Blog}  />
					<Route path='/projectplanner' component={ComingSoon} />
					<Route component={NotFound}  />
				</Switch>
			</div>
		</BrowserRouter>
		
,document.querySelector('.body'));